# @commonshost/manifest 📑

**HTTP/2 Server Push Manifest** is a declarative configuration syntax for web servers, CDNs, and tools that offers web developers access to this powerful new protocol feature.

```js
[
  {
    get: '/index.html',
    push: '**/*.{js,css}'
  }
]
```

## Documentation

- [FAQ](./docs/faq.md)
- [Manifest Syntax](./docs/format.md)
- [CLI tool usage](./docs/cli.md)
- [Node.js API usage](./docs/api.md)

## Supported Platforms

A manifest is an intermediary format which can be transpiled to the configuration syntax of CDN services or web servers.

**CDN services**: Commons Host, Cloudflare, Fastly, Netlify

**Web servers**: Apache, H2O, NGINX

**Languages**: Node.js/NPM

*Note*: Web browsers widely support HTTP/2 Server Push. The manifest is only needed on the server side.

## JSON Schema

See: [./src/schema.js](./src/schema.js)

## Colophon

Made with 💘 by [Sebastiaan Deckers](https://twitter.com/sebdeckers) and [Sophie Yang](https://gitlab.com/sophieyang0813) for the 🐑 [Commons Host](https://commons.host) project.
