const {join} = require('path')
const {readdir} = require('fs')
const {promisify} = require('util')

async function list (...paths) {
  return promisify(readdir)(join(...paths))
}

module.exports.list = list
