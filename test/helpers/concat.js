// 😍 Readable[Symbol.asyncIterator]
//
// async function concat (stream) {
//   let string = ''
//   for await (const chunk of stream) {
//     string += chunk
//   }
//   return string
// }

function concat (stream) {
  let string = ''
  return new Promise((resolve) => {
    stream.on('data', (chunk) => { string += chunk })
    stream.on('end', () => resolve(string))
  })
}

module.exports.concat = concat
