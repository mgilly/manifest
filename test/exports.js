const test = require('blue-tape')
const api = require('..')

test('Export API surface', async (t) => {
  t.is(typeof api.schema, 'object')
  t.is(typeof api.validate, 'function')
  t.is(typeof api.normalise, 'function')
})
