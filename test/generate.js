const test = require('blue-tape')
const {join} = require('path')
const {generate, validate} = require('..')

test('Generate a manifest', async (t) => {
  const expected = [
    {
      get: '/dummy.css',
      push: [
        { priority: 256, glob: [ '/high_priority.css' ] },
        { priority: 128, glob: [ '/high_priority.woff2' ] },
        { priority: 1, glob: [ '/medium_priority.svg' ] }
      ]
    }
  ]
  const actual = await generate(join(__dirname, 'fixtures/generate'))
  t.deepEqual(actual, expected)
  t.doesNotThrow(() => validate(actual))
})
