paths:
  "/index.html":
    mruby.handler: |
      Proc.new do |env|
        [399, {"link" => <<~eos
          </app.js>; rel=preload
          </design.css>; rel=preload
        eos
        }, []]
      end
