const manifest = new Map([
  ['/', '</app.js>; rel=preload; as=script, </design.css>; rel=preload; as=style']
])

self.addEventListener('fetch', (event) => {
  event.respondWith(respond(event))
})

async function respond (event) {
  const response = await self.fetch(event.request)
  const {pathname} = new URL(event.request.url)
  if (manifest.has(pathname)) {
    const cloned = new Response(response.body, {
      status: response.status,
      statusText: response.statusText,
      url: response.url,
      headers: new Headers()
    })
    for (const [field, value] of response.headers) {
      cloned.headers.set(field, value)
    }
    const preload = manifest.get(pathname)
    cloned.headers.set('link', preload)
    return cloned
  } else {
    return response
  }
}
