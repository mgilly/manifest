sub vcl_recv {
#FASTLY recv
  if (fastly_info.is_h2 && req.url ~ "^/")
  {
     h2.push("/app.js");
     h2.push("/design.css");
  }
}
