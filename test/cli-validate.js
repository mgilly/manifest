const test = require('blue-tape')
const {spawn} = require('child_process')
const {join, resolve} = require('path')
const {once} = require('./helpers/once')
const {concat} = require('./helpers/concat')

const node = process.execPath
const cli = resolve(__dirname, '../src/cli.js')

test('Fail for invalid manifest', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const manifest = 'invalid.json'
  const args = ['--', cli, 'validate', '-m', manifest]
  const child = spawn(node, args, {cwd})
  t.equal(await concat(child.stderr), '')
  t.ok(/FAIL:/.test(await concat(child.stdout)))
  await once(child, 'close')
})

test('Pass for valid manifest', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const manifest = 'static-site-generator/expected.json'
  const args = ['--', cli, 'validate', '-m', manifest]
  const child = spawn(node, args, {cwd})
  t.equal(await concat(child.stderr), '')
  t.ok(/PASS:/.test(await concat(child.stdout)))
  await once(child, 'close')
})
