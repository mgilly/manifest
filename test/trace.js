const test = require('blue-tape')
const {trace, normalise} = require('..')
const {deepStrictEqual} = require('assert')

const PRIORITY_DEFAULT = 16
const PRIORITY_HIGH = 256
const PRIORITY_LOW = 1

const index = [
  '/index.html',
  '/foo.js',
  '/bar.js',
  '/foo.js.map',
  '/foo.svg',
  '/foo.css'
]

const fixtures = [
  {
    scenario: 'Nested dependencies',
    given: {
      entry: '/index.html',
      files: index,
      manifest: [
        {glob: '/*.html', push: '/**/foo.js'},
        {glob: '/foo.js', push: ['**/*.js', '!**/*.map']}
      ]
    },
    expected: new Map([
      ['/foo.js', PRIORITY_DEFAULT],
      ['/bar.js', PRIORITY_DEFAULT]
    ])
  },
  {
    scenario: 'Custom priority',
    given: {
      entry: '/index.html',
      files: index,
      manifest: [
        {
          glob: '/index.html',
          push: [
            {glob: '**/*.js', priority: PRIORITY_HIGH},
            {glob: '**/*.css'},
            {glob: '**/*.svg', priority: PRIORITY_LOW}
          ]
        }
      ]
    },
    expected: new Map([
      ['/foo.js', PRIORITY_HIGH],
      ['/bar.js', PRIORITY_HIGH],
      ['/foo.css', PRIORITY_DEFAULT],
      ['/foo.svg', PRIORITY_LOW]
    ])
  }
]

for (
  const {
    scenario,
    given: {manifest, files, entry},
    expected
  } of fixtures
) {
  test(scenario, async (t) => {
    const actual = trace(
      normalise(manifest),
      files,
      entry
    )
    t.doesNotThrow(() => deepStrictEqual(actual, expected))
  })
}
