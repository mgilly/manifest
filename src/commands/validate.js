const {validate} = require('../capabilities/validate')
const chalk = require('chalk')
const {readFile} = require('fs')
const {promisify} = require('util')
const {relative} = require('path')

module.exports.command = ['validate']
module.exports.aliases = []
module.exports.desc = 'Check a manifest syntax'
module.exports.builder = {
  manifest: {
    alias: 'm',
    type: 'string',
    describe: 'File path of a manifest',
    default: 'serverpush.json'
  }
}

module.exports.handler = async function handler ({manifest}) {
  try {
    const data = await promisify(readFile)(manifest)
    await validate(JSON.parse(data))
  } catch (error) {
    console.log(chalk.red(`FAIL: ${error.message}`))
    process.exit(1)
  }
  const filepath = relative(process.cwd(), manifest)
  console.log(chalk.green(`PASS: ${filepath}`))
}
