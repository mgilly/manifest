const suffix = 'index.html'

function cleanUrl (filepath) {
  if (filepath.endsWith(`/${suffix}`)) {
    return filepath.slice(0, -suffix.length)
  } else {
    return filepath
  }
}

module.exports.cleanUrl = cleanUrl
