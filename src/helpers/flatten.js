const isGlob = /(^!)|(\*)/

function flatten (manifest) {
  const results = new Map()
  for (const rule of manifest) {
    if (!Array.isArray(rule.push)) continue
    if (!Array.isArray(rule.get)) continue

    const dependencies = new Set()
    for (const push of rule.push) {
      for (const dependency of push.glob) {
        if (isGlob.test(dependency)) continue

        dependencies.add(dependency)
      }
    }

    if (dependencies.size === 0) continue

    for (const get of rule.get) {
      for (const filepath of get.glob) {
        if (isGlob.test(filepath)) continue

        const location = filepath
        if (results.has(location)) {
          const previous = results.get(location)
          const merged = new Set([...previous, ...dependencies])
          results.set(location, merged)
        } else {
          results.set(location, dependencies)
        }
      }
    }
  }
  return results
}

module.exports.flatten = flatten
