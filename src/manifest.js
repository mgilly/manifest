const {schema} = require('./schema')
const {normalise} = require('./capabilities/normalise')
const {validate} = require('./capabilities/validate')
const {generate} = require('./capabilities/generate')
const {translate} = require('./capabilities/translate')
const {trace} = require('./capabilities/trace')

class Manifest {
  constructor (manifest = []) {
    this.manifest = manifest
    if (!Array.isArray(this.manifest)) {
      throw new TypeError('Manifest must be an Array')
    }
  }

  validate () {
    return validate(this.manifest)
  }

  normalise () {
    return normalise(this.manifest)
  }
}

module.exports.schema = schema
module.exports.normalise = normalise
module.exports.validate = validate
module.exports.generate = generate
module.exports.translate = translate
module.exports.trace = trace
module.exports.Manifest = Manifest
