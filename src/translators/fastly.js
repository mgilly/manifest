const {flatten} = require('../helpers/flatten')
const {cleanUrl} = require('../helpers/cleanUrl')

async function translate (manifest) {
  let output = 'sub vcl_recv {\n#FASTLY recv\n'
  for (const [location, dependencies] of flatten(manifest)) {
    const pathname = cleanUrl(location)
    output += `  if (fastly_info.is_h2 && req.url ~ "^${pathname}")\n  {\n`
    for (const dependency of dependencies) {
      output += `     h2.push("${dependency}");\n`
    }
    output += '  }\n'
  }
  output += '}\n'
  return output
}

module.exports.translate = translate
