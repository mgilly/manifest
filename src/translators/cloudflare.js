const {flatten} = require('../helpers/flatten')
const {cleanUrl} = require('../helpers/cleanUrl')
const {requestDestination} = require('request-destination')

const head = 'const manifest = new Map([\n'

const tail = `
])

self.addEventListener('fetch', (event) => {
  event.respondWith(respond(event))
})

async function respond (event) {
  const response = await self.fetch(event.request)
  const {pathname} = new URL(event.request.url)
  if (manifest.has(pathname)) {
    const cloned = new Response(response.body, {
      status: response.status,
      statusText: response.statusText,
      url: response.url,
      headers: new Headers()
    })
    for (const [field, value] of response.headers) {
      cloned.headers.set(field, value)
    }
    const preload = manifest.get(pathname)
    cloned.headers.set('link', preload)
    return cloned
  } else {
    return response
  }
}
`

async function translate (manifest) {
  const locations = []
  for (const [location, dependencies] of flatten(manifest)) {
    const headers = []
    for (const dependency of dependencies) {
      const destination = requestDestination(dependency)
      headers.push(`<${dependency}>; rel=preload; as=${destination}`)
    }
    const pathname = cleanUrl(location)
    locations.push(`  ['${pathname}', '${headers.join(', ')}']`)
  }
  return head + locations.join(',\n') + tail
}

module.exports.translate = translate
