const {flatten} = require('../helpers/flatten')

async function translate (manifest) {
  let output = ''
  for (const [location, dependencies] of flatten(manifest)) {
    output += `location = "${location}" {\n`
    for (const dependency of dependencies) {
      output += `    http2_push "${dependency}";\n`
    }
    output += '}\n'
  }
  return output
}

module.exports.translate = translate
