const {flatten} = require('../helpers/flatten')
const {cleanUrl} = require('../helpers/cleanUrl')
const {requestDestination} = require('request-destination')

async function translate (manifest) {
  let output = ''
  for (const [location, dependencies] of flatten(manifest)) {
    output += `${cleanUrl(location)}\n`
    for (const dependency of dependencies) {
      const destination = requestDestination(dependency)
      output +=
        `  Link: <${cleanUrl(dependency)}>; ` +
        'rel=preload; ' +
        `as=${destination}\n`
    }
  }
  return output
}

module.exports.translate = translate
