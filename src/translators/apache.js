const {flatten} = require('../helpers/flatten')
const {cleanUrl} = require('../helpers/cleanUrl')

async function translate (manifest) {
  let output = ''
  for (const [location, dependencies] of flatten(manifest)) {
    output += `<Location "${cleanUrl(location)}">\n`
    for (const dependency of dependencies) {
      output += `  H2PushResource add "${cleanUrl(dependency)}"\n`
    }
    output += '</Location>\n'
  }
  return output
}

module.exports.translate = translate
