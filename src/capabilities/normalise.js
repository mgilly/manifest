// https://tools.ietf.org/html/rfc7540#section-5.3.5
// All streams are initially assigned a non-exclusive dependency on
// stream 0x0.  Pushed streams (Section 8.2) initially depend on their
// associated stream.  In both cases, streams are assigned a default
// weight of 16.
const DEFAULT_PRIORITY = 16

function isUri (value) {
  return typeof value === 'string' && value.startsWith('https://')
}

function isGlob (value) {
  return typeof value === 'string' && !value.startsWith('https://')
}

function validRule ({get, push}) {
  return push.length && get.length
}

function validLookup ({uri, glob}) {
  return (uri && uri.length) || (glob && glob.length)
}

const cached = new WeakMap()

function normalise (manifest) {
  if (cached.has(manifest)) return cached.get(manifest)

  const normalised = (manifest || []).map((rule) => {
    for (const key of ['get', 'uri', 'glob', 'push']) {
      if (typeof rule[key] === 'string') rule[key] = [rule[key]]
    }

    for (const key of ['get', 'push']) {
      if (!Array.isArray(rule[key])) {
        if (rule[key] === undefined) rule[key] = []
        else rule[key] = [rule[key]]
      }
    }

    if (rule.get.length > 0 && rule.get.every(isGlob)) {
      rule.get = [{glob: rule.get}]
    } else {
      rule.get = rule.get.map((dep) => {
        if (typeof dep === 'string') {
          if (dep.startsWith('https://')) dep = {uri: [dep]}
          else dep = {glob: [dep]}
        }
        if (typeof dep.uri === 'string') dep.uri = [dep.uri]
        if (typeof dep.glob === 'string') dep.glob = [dep.glob]
        return dep
      }).filter(validLookup)
    }

    for (const key of ['uri', 'glob']) {
      if (key in rule) {
        if (rule[key].length > 0) {
          rule.get.push({[key]: rule[key]})
        }
        delete rule[key]
      }
    }

    if (rule.push.length > 0 && rule.push.every(isGlob)) {
      rule.push = [{
        priority: DEFAULT_PRIORITY,
        glob: Array.from(rule.push)
      }]
    } else {
      rule.push = rule.push.map((dep) => {
        if (Array.isArray(dep)) {
          const uris = dep.filter(isUri)
          const globs = dep.filter(isGlob)
          dep = {}
          if (uris.length) dep.uri = uris
          if (globs.length) dep.glob = globs
        }
        if (typeof dep === 'string') {
          if (dep.startsWith('https://')) dep = {uri: [dep]}
          else dep = {glob: [dep]}
        }
        if (typeof dep.uri === 'string') dep.uri = [dep.uri]
        if (typeof dep.glob === 'string') dep.glob = [dep.glob]
        if (typeof dep.priority !== 'number') dep.priority = DEFAULT_PRIORITY
        return dep
      }).filter(validLookup)
    }
    return rule
  }).filter(validRule)

  if (manifest) cached.set(manifest, normalised)

  return normalised
}

module.exports.normalise = normalise
