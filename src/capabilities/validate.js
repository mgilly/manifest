const Ajv = require('ajv')
const {schema} = require('../schema')
const {extglobFormat} = require('../format/extglob')

const ajv = new Ajv({format: 'full'})
ajv.addFormat('extglob', extglobFormat)
ajv.addSchema(schema)

module.exports.validate =
function validate (json) {
  const validity = ajv.validate('/Manifest', json)
  if (validity === true) {
    return true
  } else {
    const last = ajv.errors.pop()
    const message = `${last.dataPath} ${last.message} ${last.schemaPath}`
    throw new Error(message)
  }
}
