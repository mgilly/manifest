const micromatch = require('micromatch')
const {normalise} = require('./normalise')

function trace (manifest, filepaths, entry, dependencies = new Map()) {
  _trace(normalise(manifest), filepaths, entry, dependencies)
  return dependencies
}

function _trace (manifest, filepaths, entry, dependencies) {
  for (const rule of manifest) {
    for (const get of rule.get) {
      if (micromatch.any(entry, get.glob)) {
        for (const push of rule.push) {
          const deps = micromatch(filepaths, push.glob)
          for (const dep of deps) {
            if (!dependencies.has(dep)) {
              dependencies.set(dep, push.priority)
              _trace(manifest, filepaths, dep, dependencies)
            }
          }
        }
      }
    }
  }
}

module.exports.trace = trace
