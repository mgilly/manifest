const {normalise} = require('../capabilities/normalise')

const translators = new Map([
  // Servers
  ['nginx', require('../translators/nginx')],
  ['apache', require('../translators/apache')],
  ['h2o', require('../translators/h2o')],

  // CDNs
  ['cloudflare', require('../translators/cloudflare')],
  ['fastly', require('../translators/fastly')],
  ['netlify-toml', require('../translators/netlify-toml')],
  ['netlify-headers', require('../translators/netlify-headers')]
])

async function translate (manifest, format) {
  if (!translators.has(format)) {
    throw new Error(`Format not supported: ${format}`)
  }

  const translator = translators.get(format)
  const normalised = normalise(manifest)
  const output = await translator.translate(normalised)
  return output
}

module.exports.translate = translate
