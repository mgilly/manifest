## Manifest Format

The idea is simple: specify a list of **trigger and action tuples**. The syntax is flexible and forgiving, with shorthand notation for ease of use.

A **manifest** is a JSON array containing rules.

```js
[]
```

Each **rule** is a JSON object containing a `get` and `push` property. These are the **trigger** and **action** respectively.

```js
[
  {get, push},
  {get, push}, ...
]
```

**Triggers** are *URI Template* patterns or file path *globs*. They match the response's pathname, or the resource on disk (in the case of static sites).

```js
[
  {get: '/index.html', push},
  {get: 'https://example.net/', push}, ...
]
```

**Actions** are the resources to be pushed. These are also file path *globs* or *URI templates*.

```js
[
  {get: '/index.html', push: '/app.js'},
  {get: '/app.js', push: '/lib.js'}, ...
]
```

**Globs**, and **extglobs**, are POSIX/Bash-compatible expressions to match file paths. They support wildcards, negations, expansions, and more. See [micromatch](https://github.com/micromatch/micromatch).

```js
[
  {get: '/index.html',
   push: '/{css,js}/**/*'}, ...
]
```

Note: Paths, for both `get` and `push`, must start with a slash (`/`) or globstar (`**`), optionally preceded by `!` to negate the pattern.

```js
[{get: [
  // Invalid: Does not match any files.
  'index.html',

  // Valid: Matches exactly one file in the root path.
  '/index.html',

  // Valid: Matches exactly one file in a subdirectory.
  '/some/where/index.html',

  // Valid: Matches any index.html file,
  // in all directories (including the root).
  '**/index.html',

  // Valid: Matches all files with the `html` or `htm` extensions.
  '**/*.{html,htm}'

  // Valid: Exclude a file from the resulting set.
  '!/robots.txt'
]}]
```

**URI Templates** are strings formatted to match URLs. They support for pathname expansion, order-independent query string fields, and wildcards. See [RFC 6570](https://tools.ietf.org/html/rfc6570). They must start with `http://` or `https://`.

```js
[
  {get: 'https://example.net/shop{/brand}',
   push: 'https://example.net/banners{/brand}.png'},
  {get: 'https://example.net{/topic}',
   push: 'https://api.example.net/topics{/productId}.json'}, ...
]
```

In **shorthand** notation, both actions and triggers can be expressed as JSON strings, or arrays of strings.

```js
[
  {get: ['/*.html', '!/legacy.html'],
   push: ['/images/*.{png,jpg}', '!/images/huge.jpg']}, ...
]
```

In **normalised** notation, they are arrays containing objects with `uri` and/or `glob` properties, which are in turn either strings or arrays of strings.

The **priority** number is a property of the normalised push action and ranges from `1` to `256`. Defaults to `16` as per the [HTTP/2 specification](https://http2.github.io/http2-spec/#PRIORITY).

```js
[
  {
    get: [
      {
        uri: [
          'https://example.net/',
          'https://example.net/about', ...
        ],
        glob: [
          '/images/*.{jpg,webp}',
          '/styles/**/*.css', ...
        ]
      }, ...
    ],
    push: [
      {
        glob: [
          '/fonts/*.ttf', ...
        ],
        uri: [
          'https://cdn.example.net/logo.svg', ...
        ],
        priority: 100
      }, ...
    ]
  }, ...
]
```

Take a look at the [test cases](./test) for more normalisation examples.
