## CLI

Intended as a front-end build tool in continuous integration & deployment.

```
npm install @commonshost/manifest
```

### Generate a Manifest

*Command*: `generate`

Traces the dependencies of all HTML, CSS, and JS files in a directory and creates a server push manifest JSON file.

```
manifest generate [-r|--root <PATH>] [-m|--manifest <FILE>]
```

```
manifest generate --root /var/www --manifest serverpush.json
```

#### Option: `--root <PATH>`

*Alias*: `-r`

*Default*: Auto-detect static site generator (e.g. `./dist`, `./public`, ...), otherwise use the current working directory (`.`).

The path to a directory containing a static site. All HTML, CSS, and JS files will be parsed to trace their dependencies.

#### Option: `--manifest <FILE>`

*Alias*: `-m`

*Default*: `./serverpush.json`

The path to a file where the manifest will be written.

If set to an empty string (i.e. `--manifest ''`) the manifest is directed to the standard output (STDOUT). This allows piping to other tools on the command line or in shell scripts.

The default file name is the same as the default server configuration manifest loaded by [@commonshost/server](https://npmjs.com/package/@commonshost/server). This makes it easy to run both commands during local development.

```
$ npx @commonshost/manifest generate
$ npx @commonshost/server start
```

### Validate a Manifest

*Command*: `validate`

Checks the syntax of a server push manifest file according to the JSON schema.

If the manifest is valid, prints `PASS` with the path to the validated manifest file. Otherwise prints `FAIL` with a validation error message, and exits with a non-zero error code.

```
manifest validate [-m|--manifest <FILE>]
```

```
manifest validate --manifest serverpush.json
```

#### Options: `--manifest <FILE>`

*Alias*: `-m`

*Default*: `./serverpush.json`

The path to a manifest file to be read and validated.

### Translate a Manifest

*Command*: `translate`

*Aliases*: `transpile`, `convert`, `export`

Turn a manifest into a configuration file for web servers or CDN services.

```
manifest translate [-m|--manifest <FILE>] [-f|--format <FORMAT>] [-o|--output <PATH>]
```

```
manifest translate --manifest serverpush.json --format apache --output httpd.conf
```

```
manifest translate --manifest serverpush.json --format apache
```

#### Option: `--manifest <FILE>`

*Alias*: `-m`

*Default*: `./serverpush.json`

The path to a manifest file to be read and translated.

If set to an empty string (i.e. `--manifest ''`) the manifest is read from the standard input (STDIN). This allows piping from other tools on the command line or in shell scripts.

#### Option: `--format <FORMAT>`

*Alias*: `-f`

*Choices*: `apache`, `h2o`, `nginx`, `cloudflare`, `fastly`, `netlify-headers`, `netlify-toml`

See: [`translate()` API](./api.md) documentation for examples of the generated instructions for each target web server or CDN service.

#### Option: `--output`

*Alias*: `-o`

*Default*: Print to the standard output (STDOUT)

The path to an output file.

For example: `httpd.conf` (Apache), `nginx.conf` (NGINX), `netlify.toml` (Netlify), `worker.js` (Cloudflare), etc.

If set to an empty string (i.e. `--output ''`) the output is directed to the standard output (STDOUT). This allows piping to other tools on the command line or in shell scripts.
